import 'dart:convert';

import 'package:flapkap/Models/Order.dart';
import 'package:flapkap/bloc/bloc.dart';
import 'package:flapkap/bloc/home/home_event.dart';
import 'package:flapkap/bloc/home/home_state.dart';
import 'package:rxdart/rxdart.dart';
import 'package:flutter/services.dart' show rootBundle;

class HomeBloc extends BLoC<HomeEvent> {
  final PublishSubject homeStateSubject = PublishSubject<HomeState>();

  @override
  void dispatch(HomeEvent event) async {
    if (event is OrdersListRequested) {
      await _getHomeScreenData();
    }
  }

  Future<void> _getHomeScreenData() async {
    String response = await rootBundle.loadString('assets/orders.json');
    final data = await json.decode(response);
    List<Order> orders =
        List<Order>.from(data.map((model) => Order.fromJson(model)));
    homeStateSubject.sink.add(OrdersAre(orders));
  }
}
