// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'Order.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

Order _$OrderFromJson(Map<String, dynamic> json) => Order(
      json['id'] as String?,
      json['isActive'] as bool?,
      json['price'] as String?,
      json['company'] as String?,
      json['picture'] as String?,
      json['buyer'] as String?,
      (json['tags'] as List<dynamic>?)?.map((e) => e as String).toList(),
      json['status'] as String?,
      json['registered'] == null
          ? null
          : DateTime.parse(json['registered'] as String),
    )..querys =
        (json['querys'] as List<dynamic>?)?.map((e) => e as String).toList();

Map<String, dynamic> _$OrderToJson(Order instance) => <String, dynamic>{
      'querys': instance.querys,
      'id': instance.id,
      'isActive': instance.isActive,
      'price': instance.price,
      'company': instance.company,
      'picture': instance.picture,
      'buyer': instance.buyer,
      'tags': instance.tags,
      'status': instance.status,
      'registered': instance.registered?.toIso8601String(),
    };
