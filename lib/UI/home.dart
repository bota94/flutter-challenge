import 'dart:async';

import 'package:flapkap/Models/Order.dart';
import 'package:flapkap/bloc/home/home_bloc.dart';
import 'package:flapkap/bloc/home/home_event.dart';
import 'package:flapkap/bloc/home/home_state.dart';
import 'package:flutter/material.dart';

class HomePage extends StatefulWidget {
  const HomePage({
    Key? key,
  }) : super(key: key);

  @override
  State<HomePage> createState() => _HomePageState();
}

class _HomePageState extends State<HomePage> {
  HomeBloc homeBloc = HomeBloc();
  StreamSubscription? homeSub;
  List<Order> orders = [];
  int? totalOrdersNumber;
  int? returnedOrdersNumber;
  double? averagePrice;

  @override
  void initState() {
    homeSub = homeBloc.homeStateSubject.listen((state) {
      if (state is OrdersAre) {
        setState(() {
          orders = state.orders;
        });
        getRequiredData();
      }
    });
    homeBloc.dispatch(OrdersListRequested());
    super.initState();
  }

  void getRequiredData() {
    totalOrdersNumber = orders.length;
    List<Order> returnedOrders =
        orders.where((order) => order.status == "RETURNED").toList();
    returnedOrdersNumber = returnedOrders.length;
    double totalPrice = 0;
    for (Order order in orders) {
      String price = order.price?.substring(1) ?? '';
      double orderPrice = double.tryParse(price.replaceAll(',', '')) ?? 0;
      totalPrice += orderPrice;
    }
    averagePrice = totalPrice / orders.length;
    setState(() {});
  }

  @override
  void dispose() {
    homeSub?.cancel();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar:AppBar(
        title:const Text("FlapKap challenge")
      ),
      body: Center(
        child: Padding(
          padding: const EdgeInsetsDirectional.only(start:12.0),
          child: Row(
            children: [
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.start,
                children: const [
                  Text(
                    "Total number of orders : ",
                  ),
                  Padding(
                    padding: EdgeInsets.symmetric(vertical: 8.0),
                    child: Text("Total number of returned orders : "),
                  ),
                  Text("Orders average price : "),
                ],
              ),
              Column(
                mainAxisAlignment: MainAxisAlignment.center,
                crossAxisAlignment: CrossAxisAlignment.end,
                children: [
                  Text(
                    "$totalOrdersNumber",
                  ),
                  Padding(
                    padding: const EdgeInsets.symmetric(vertical: 8.0),
                    child: Text("$returnedOrdersNumber"),
                  ),
                  Text("${averagePrice?.toStringAsFixed(3)}"),
                ],
              ),
            ],
          ),
        ),
      ),
    );
  }
}
