import 'dart:async';

import 'package:flapkap/Models/Order.dart';
import 'package:flapkap/bloc/home/home_bloc.dart';
import 'package:flapkap/bloc/home/home_event.dart';
import 'package:flapkap/bloc/home/home_state.dart';
import 'package:flutter/material.dart';

class GraphScreen extends StatefulWidget {
  const GraphScreen({Key? key}) : super(key: key);

  @override
  _GraphScreenState createState() => _GraphScreenState();
}

class _GraphScreenState extends State<GraphScreen> {
  List<Order> orders = [];
  HomeBloc homeBloc = HomeBloc();
  StreamSubscription? homeSub;
  @override
  void initState() {
    homeSub = homeBloc.homeStateSubject.listen((state) {
      if (state is OrdersAre) {
        setState(() {
          orders = state.orders;
        });
      }
    });
    homeBloc.dispatch(OrdersListRequested());
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: const Text("FlapKap challenge")),
    );
  }
}
