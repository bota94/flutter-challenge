import 'package:flapkap/ui/graph_screen.dart';
import 'package:flapkap/ui/home.dart';
import 'package:flutter/material.dart';

class NavigationBar extends StatefulWidget {
  const NavigationBar({Key? key}) : super(key: key);

  @override
  _NavigationBarState createState() => _NavigationBarState();
}

class _NavigationBarState extends State<NavigationBar> {
  int? _current;
  Widget? _body;

  Map<int, Widget> screenMap = {
    0: const HomePage(),
    1: const GraphScreen(),
  };

  @override
  void initState() {
    setState(() {
      _current = 0;
      _body = screenMap[_current];
    });
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      bottomNavigationBar: _bottomNavBar(),
      body: _body,
    );
  }

  setScreen(int i) {
    setState(() {
      _current = i;
      _body = screenMap[i];
    });
  }

  BottomNavigationBar _bottomNavBar() {
    return BottomNavigationBar(
        type: BottomNavigationBarType.fixed,
        backgroundColor: Colors.white,
        selectedItemColor: Colors.blue,
        unselectedItemColor: Colors.grey,
        currentIndex: _current ?? 0,
        onTap: (int i) {
          setScreen(i);
        },
        items: <BottomNavigationBarItem>[
          BottomNavigationBarItem(
              icon: Icon(
                Icons.home,
                color: _current == 0 ? Colors.blue : Colors.grey,
              ),
              label: "Home Page"),
          BottomNavigationBarItem(
              icon: Icon(
                Icons.bar_chart,
                color: _current == 1 ? Colors.blue : Colors.grey,
              ),
              label: "Graph Screen"),
        ]);
  }
}
